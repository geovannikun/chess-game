# Chess Game

This project simulate knight chess piece movement in two turns

## Getting Started

This project is all developed with nodejs ans is separeted in two folters, `backend` and `frontend`.   
Each have their own package.json

### Prerequisites

As Prerequisite you need nodejs 8 and npm

### Installing

To each project, exeute the folowing commands:

```
npm install
npm start
```

## Running the tests

To tun tests, execute:
```
npm run tests
```

## Contributing

Please read [CONTRIBUTING.md](https://bitbucket.org/geovannikun/chess-game/src/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.