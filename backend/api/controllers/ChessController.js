'use strict';

var util = require('util');


class KnightPosition {
  constructor(x, y, turns = 0) {
    this.x = x;
    this.y = y;
    this.position = this.toString();
    if(turns > 0)
      this.getPossibleMoves(--turns)
  }

  getPossibleMoves(turns) {
    this.possible_positions = [];
    [[1, 2], [2, 1]].forEach(([xVariation, yVariation]) => {
      [[-1, -1], [1, -1], [-1, 1], [1, 1]].forEach(([xOperation, yOperation]) => {
        const xPos = this.x + (xOperation * xVariation);
        const yPos = this.y + (yOperation * yVariation);
        if(
          xPos > 0 && xPos < 9
          &&
          yPos > 0 && yPos < 9
        )
          this.possible_positions.push(new KnightPosition(xPos, yPos, turns));
      })
    })
  }

  toString() {
    return `${String.fromCharCode(64 + this.x)}${this.y}`;
  }
}

const CheesController = {
  moves(req, res) {
    const piece = req.swagger.params.piece.value;
    const position = req.swagger.params.position.value;
    const turns = req.swagger.params.turns.value || 1;
    const posCol = position.split('')[0].charCodeAt(0) - 64;
    const posRow = parseInt(position.split('')[1]);
    if(piece!='knight'){
      res.statusCode = 500;
      res.json({
        message: 'Wrong piece'
      });
      return;
    }
    if(
      position.length != 2 ||
      posRow > 8 ||
      posCol > 8 || posCol < 1
    ) {
      res.statusCode = 500;
      res.json({
        message: 'Wrong position'
      });
      return;
    }
  
    res.json(new KnightPosition(posRow, posCol, turns));
  }
}

module.exports = CheesController;
