var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function() {

  describe('ChessController', function() {

    describe('GET /moves', function() {

      it('should return possible movement to 1 turn', function(done) {

        request(server)
          .get('/api/moves')
          .query({
            piece: 'knight',
            position: 'H8',
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);

            res.body.should.eql({
              "x": 8,
              "y": 8,
              "position": "H8",
              "possible_positions": [
                {
                  "x": 7,
                  "y": 6,
                  "position": "G6"
                },
                {
                  "x": 6,
                  "y": 7,
                  "position": "F7"
                }
              ]
            });

            done();
          });
      });

      it('should return possible movement to 2 turn', function(done) {

        request(server)
          .get('/api/moves')
          .query({
            piece: 'knight',
            position: 'H8',
            turns: 2,
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);
            should(res.body.possible_positions[0].possible_positions).be.an.Array();

            done();
          });
      });

      it('should get error with wrong piece', function(done) {

        request(server)
          .get('/api/moves')
          .query({
            piece: 'king',
            position: 'H8',
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', '/json/')
          .expect(200)
          .end(function(err, res) {
            should.exist(err);

            res.body.should.eql({ message: 'Wrong piece'});

            done();
          });
      });

      it('should get error with wrong position', function(done) {

        request(server)
          .get('/api/moves')
          .query({
            piece: 'knight',
            position: 'H9',
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.exist(err);

            res.body.should.eql({ message: 'Wrong position'});

            done();
          });
      });

    });

  });

});
