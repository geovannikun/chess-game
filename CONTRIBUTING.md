## We Develop with Bitbucket
We use Bitbucket to host code, to track issues and feature requests, as well as accept pull requests.

## How to contribute

1. Clone the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. Any change to API, need reflect in swagger model.
4. Ensure the test suite passes.
5. Issue that pull request!

## Report bugs using Bitbucket's [issues](https://bitbucket.org/geovannikun/chess-game/issues)
We use Bitbucket issues to track public bugs. Report a bug by [opening a new issue](https://bitbucket.org/geovannikun/chess-game/issues/new); it's that easy!

## Board
We use trello board integrated to bitbucket, with that you can follow and help project development.

## Technologies
If you wanna contribute, those are the technologies used in project:

- Backend is developed using swagger with restify integrated and tests using supertest.
- Frontend is developed using React + Typescript and tests with jest.