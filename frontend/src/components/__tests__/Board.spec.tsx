import * as React from 'react';
import { shallow } from 'enzyme';
import Board from '../Board';
import * as fetchMock from 'fetch-mock';
const ChessApiMovesResult = require('./ChessApiMovesResult.json');

describe('<Board />', () => {
  let boardComporent;
  beforeEach(()=> {
    boardComporent = shallow(<Board />);
  })
  
  it('should render without crashing', () => {
    expect(boardComporent).toBeTruthy();
  });
  
  it('should render every cell', () => {
    expect(boardComporent.find('.board-row')).toHaveLength(8);
    expect(boardComporent.find('.board-col')).toHaveLength(8*8);
  });

  it('should render on cell click', (done) => {
    const firstCell = boardComporent.find('.board-col').first();
    fetchMock.mock(`http://localhost:10010/api/moves?piece=knight&position=${firstCell.text()}&turns=2`,  {
      status: 200,
      headers: {'Content-Type': 'application/json'},
      body: ChessApiMovesResult
    });
    firstCell.simulate('click');
    setTimeout(() => {
      boardComporent.update();
      expect(boardComporent.find('.board-col.checked')).toHaveLength(16);
      done();
    }, 50);
  });
});