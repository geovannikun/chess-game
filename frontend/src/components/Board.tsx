import * as React from "react";
import { Component } from 'react';
import './Board.scss';
import ChessApi from '../Utils/ChessApi';

export interface BoardCell {
    code: string;
    checked: boolean;
}

export interface BoardState {
    size: number,
    cells: Array<Array<BoardCell>>
}

class Board extends Component<any, BoardState> {
    state = {
        size: 8,
        cells: [],
    }

    constructor(state, props) {
        super(state, props);
        this.state = ({
            ...this.state,
            cells: this.createBoardCells(),
        });
    }

    cellClickHandler = (cell:BoardCell) => async (e) => {
        const moves = await ChessApi.getMoves('knight', cell.code, 2);
        const newBoard = [...this.state.cells];
        newBoard.forEach(row => {
            row && row.forEach(col => {
                col.checked = false;
            })
        })
        moves.possible_positions.forEach(move => {
            newBoard[move.x][move.y].checked = true;
            move.possible_positions.forEach(move => {
                newBoard[move.x][move.y].checked = true;
            });
        });
        this.setState({
            ...this.state,
            cells: newBoard,
        })
    }

    createBoardCells(): Array<Array<BoardCell>> {
        const board = new Array();
        for(let row = this.state.size; row > 0; row--) {
            for(let col = 1; col <= this.state.size; col++) {
                board[row] = board[row] || [];
                board[row][col] = {
                    code: `${String.fromCharCode(64 + col)}${row}`,
                    checked: false,
                } as BoardCell;
            }
        }
        return board;
    }

    render() {
        return (
            <div className="board">
                {this.state.cells.map((row, index) => (
                    <div className="board-row" key={index}>
                        {row && row.map((col, index) => (
                            <button className={`board-col ${col.checked ? 'checked' : ''}`} key={col.code} onClick={this.cellClickHandler(col)}>
                                <p>{col.code}</p>
                            </button>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default Board;