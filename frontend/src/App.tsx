import * as React from "react";
import { Component } from 'react';
import Board from './components/Board';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Board/>
            </div>
        );
    }
}

export default App;