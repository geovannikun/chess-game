import * as React from 'react';
import { shallow } from 'enzyme';
import ChessApi from '../ChessApi';
import * as fetchMock from 'fetch-mock';


describe('ChessApi', () => {
  
  it('should call chess api with all parameters with success', async () => {
    fetchMock.mock('http://localhost:10010/api/moves?piece=knight&position=D2&turns=2',  {
      status: 200,
      headers: {'Content-Type': 'application/json'},
      body: {hello: 'world'}
    });
    const moves = await ChessApi.getMoves('knight', 'D2', 2);
    expect(moves.hello).toEqual("world");
  });
  
  it('should call chess api with 2 parameters with success', async () => {
    fetchMock.mock('http://localhost:10010/api/moves?piece=knight&position=D2&turns=1',  {
      status: 200,
      headers: {'Content-Type': 'application/json'},
      body: {hello: 'world'}
    });
    const moves = await ChessApi.getMoves('knight', 'D2');
    expect(moves.hello).toEqual("world");
  });
});