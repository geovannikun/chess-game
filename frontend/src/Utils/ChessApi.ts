export default class ChessApi {
    static url = 'http://localhost:10010/api';
    static getMoves(piece, position, turns = 1){
        const data = {
            piece,
            position,
            turns
        }
        return fetch(`${ChessApi.url}/moves?${ChessApi.objectToQuery(data)}`, {mode: 'cors'})
            .then(response => response.json())
    }
    static objectToQuery(obj) {
        return Object.keys(obj).reduce(function(a,k){a.push(k+'='+encodeURIComponent(obj[k]));return a},[]).join('&');
    }
}